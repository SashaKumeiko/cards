document.addEventListener("DOMContentLoaded", async event => {
  let board = new Board();
  if (localStorage.getItem('token')) {
    await Visit.addCardsFromServer()
  } else {
  const user = new User('PWR.2800.59@gmail.com', '04035117')
  await user.authorization();
  await Visit.addCardsFromServer();
  }
});

class User {
  constructor(email, password) {
    this.email = email;
    this.password = password;
  }

  async authorization() {
    return fetch('http://cards.danit.com.ua/login', {
      method: 'POST',
      body: JSON.stringify({
        'email': `${this.email}`,
        'password': `${this.password}`
      })
    })
      .then(res => res.json())
      .then(resJson => {
        localStorage.setItem('token', `${resJson.token}`)
      });
  }
}

class Board {
  constructor() {
    let plate = document.querySelector(".plate")

    plate.addEventListener("dragover", function (event) {
      event.preventDefault();
      event.dataTransfer.dropEffect = "move";
    });

    plate.addEventListener("drop", this.drop)

    document
      .querySelector(".show")
      .addEventListener("click", this.showModal.bind(this));
    this.counterOfVisits = 0;
    this.visits = {};
    let modal = document.getElementById("myModal");

    modal
      .querySelector(".create-post-cardio")
      .addEventListener("click", this.createVisit.bind(this));
    modal
      .querySelector(".create-post-therapist")
      .addEventListener("click", this.createVisit.bind(this));
    modal
      .querySelector(".create-post-stomatologist")
      .addEventListener("click", this.createVisit.bind(this));
    modal
      .querySelector(".select-doctor")
      .addEventListener("change", this.selectDoctor.bind(this));
  }

  showModal(event) {
    let modal = document.getElementById("myModal");
    modal.querySelector(".comments").value = "";
    modal.style.display = "block";

    let closeModal = document.getElementsByClassName("close")[0];
    closeModal.addEventListener("click", function () {
      modal.style.display = "none";
    });

    modal.querySelectorAll("input").forEach(el => (el.value = ""));
  }
  selectDoctor(e) {
    let modal = document.getElementById("myModal");
    let selectedDoctor = e.target.value;

    switch (selectedDoctor) {
      case "Кардіолог":
        modal.querySelector(".cardio").style.display = "flex";
        modal.querySelector(".therapist").style.display = "none";
        modal.querySelector(".stomatologist").style.display = "none";

        break;

      case "Терапевт":
        modal.querySelector(".therapist").style.display = "flex";
        modal.querySelector(".stomatologist").style.display = "none";
        modal.querySelector(".cardio").style.display = "none";

        break;
      case "Стоматолог":
        modal.querySelector(".stomatologist").style.display = "flex";
        modal.querySelector(".cardio").style.display = "none";
        modal.querySelector(".therapist").style.display = "none";

        break;
    }
  }

  createVisit(ev) {
    let counter = 1;
    let emptyFields = false;
    let dataToSend = {};
    ev.target.parentElement.querySelectorAll(".checkValue").forEach(el => {
      if (
        el.value === "" &&
        counter === 1 &&
        !el.classList.contains("comments")
      ) {
        alert("Заповніть всі поля");
        counter++;
        emptyFields = true;
      } else {
        dataToSend[el.dataset.key] = el.value.toString();
      }
    });
    counter = 0;
    if (emptyFields === true) {
      // return false
    } else {
      let doc = ev.target.parentElement.dataset.doc;

      ev.target.parentElement.parentElement.parentElement.parentElement.style.display =
        "none"; //modal
      dataToSend["specialization"] = doc;
      this.createInstance(dataToSend);
    }
  }

  createInstance(dataToSend) {
    switch (dataToSend["specialization"]) {
      case "therapist":
        const therInst = new Therapist(dataToSend);
        console.log(therInst)
        therInst.addCardOnServer();
        break;
      case "stomatologist":
        const stomInst = new Stomatologist(dataToSend);
        stomInst.addCardOnServer();
        break;
      case "cardiologist":
        const cardInst = new Cardiologist(dataToSend);
        console.log(cardInst)
        cardInst.addCardOnServer();
        break;
    }
  }
}

class Visit {
  constructor(options) {
    let { title, name, description, specialization, id } = options;
    this.date = Date();
    this.title = title;
    this.name = name;
    this.description = description;
    this.specialization = specialization;
    this.id = id
  }

  static deleteCardFromServer(id) {
    return fetch(`http://cards.danit.com.ua/cards/${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    });
  }

  static addCardsFromServer() {
    return fetch('http://cards.danit.com.ua/cards', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    })
      .then(res => res.json())
      .then(parsedRes => {
        console.log('parsedRes', parsedRes);
        parsedRes.forEach(elem => {
          switch (elem["specialization"]) {
            case "therapist":
              const TherapInst = new Therapist(elem);
              console.log(TherapInst)
              TherapInst.addCardOnBoard();
              break;
            case "stomatologist":
              const stomInst = new Stomatologist(elem);
              stomInst.addCardOnBoard();
              break;
            case "cardiologist":
              const cardInsti = new Cardiologist(elem);
              cardInsti.addCardOnBoard();
              break;
          }
        });
      });
  }

  static editCardOnServer(obj) {
    const requestPut = fetch(`http://cards.danit.com.ua/cards/${obj.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(obj)
    });

    requestPut
      .then(responce => responce.json())
      .then(res => console.log(res));
  }

  editCard(event) {

    let cardMoreDiv = event.target.parentElement;
    let card = event.target.parentElement.parentElement;
    let doctor = card.querySelector(".card-doctor").innerHTML;
    card.style.display = "flex";
    card.style.flexDirection = "column";
    card.style.alignItems = "flex-end";
    card.querySelector(".card-close-btn").style.left = "5px";
    cardMoreDiv.style.display = "none";

    this.fields.map(field => {
      if (field.label) {
        let val = field.str;
        let input;
        if (field.label === 'description') {
          input = document.createElement("textarea");
        }
        else {
          input = document.createElement("input");
        }
        input.classList.add(`input-${field.label}`);
        if (field.label === 'date') {
          input.type = "date";
        }
        input.value = val;
        let label = document.createElement("label");
        label.innerHTML = `${field.label}`;
        label.appendChild(input);
        card.appendChild(label);
      }
    })

    let saveBtn = document.createElement("button");
    saveBtn.innerHTML = "save";
    saveBtn.classList.add("btn");
    saveBtn.classList.add("bg-info");
    saveBtn.classList.add("text-white");
    saveBtn.addEventListener("click", this.saveinfo.bind(this));
    card.append(saveBtn);
  }

  saveinfo(event) {
    let card = event.target.parentElement;
    let doctor = card.querySelector(".card-doctor").innerHTML;
    console.log(doctor)
    let options = {};
    options["specialization"] = doctor.toLowerCase();
    options["id"] = card.dataset.id;
    this.fields.map(field => {
      if (field.label) {
        let input = card.querySelector(`.input-${field.label}`);
        card.querySelector(`.card-${field.label}`).innerHTML = `${field.label}: ${input.value}`
        field.str = input.value
        options[`${field.label}`] = input.value
      }
    })

    Visit.editCardOnServer(options);

    card.querySelector(".card-more_info").style.display = "block";
    card.querySelectorAll("input").forEach(el => {
      el.remove();
    });
    card.querySelector("textarea").remove();
    card.querySelectorAll("label").forEach(el => el.remove());
    card.querySelector(".bg-info").remove();
  }

  static deleteCard(event) {
    let card = event.currentTarget.parentElement;
    let id = card.dataset.id;
    console.log(id)
    event.currentTarget.parentElement.remove();
    Visit.deleteCardFromServer(id);
  }

  static showMoreInfo() {
    const moreInfoBtn = document.querySelectorAll(".card-more_info-btn");
    moreInfoBtn.forEach(elem =>
      elem.addEventListener("click", function () {
        this.nextElementSibling.style.display = "block";
        this.style.display = "none";
      })
    );
  }

  static drag(dragItem) {
    let container = document.querySelector(".plate");
    let active = false;
    let currentX;
    let currentY;
    let initialX;
    let initialY;
    let xOffset = 0;
    let yOffset = 0;
    dragItem.addEventListener("touchstart", dragStart, false);
    container.addEventListener("touchend", dragEnd, false);
    container.addEventListener("touchmove", drag, false);
    dragItem.addEventListener("mousedown", dragStart, false);
    container.addEventListener("mouseup", dragEnd, false);
    container.addEventListener("mousemove", drag, false);

    function dragStart(e) {

      if (e.type === "touchstart") {
        initialX = e.touches[0].clientX - xOffset;
        initialY = e.touches[0].clientY - yOffset;
      } else {
        initialX = e.clientX - xOffset;
        initialY = e.clientY - yOffset;
      }
      if (e.currentTarget === dragItem) {
        active = true;
      }
    }
    function dragEnd(e) {
      initialX = currentX;
      initialY = currentY;
      active = false;
    }
    function drag(e) {
      if (active) {
        e.preventDefault();
        let dragoffset = {
          x: 0,
          y: 0
        };
        let offsetX
        let offsetY
        if (e.pageX - dragoffset.x < 0) {
          offsetX = 0;
        } else if (e.pageX - dragoffset.x + 102 > document.body.clientWidth) {
          offsetX = document.body.clientWidth - 102;
        } else {
          offsetX = e.pageX - dragoffset.x;
        }
        // top/bottom constraint   
        if (e.pageY - dragoffset.y < 0) {
          offsetY = 0;
        } else if (e.pageY - dragoffset.y + 10 > document.body.clientHeight) {
          offsetY = document.body.clientHeight - 10;
        } else {
          offsetY = e.pageY - dragoffset.y;
        }
        if (e.type === "touchmove") {
          currentX = e.touches[0].clientX - initialX;
          currentY = e.touches[0].clientY - initialY;
        } else {
          currentX = e.clientX - initialX;
          currentY = e.clientY - initialY;
        }
        xOffset = currentX;
        yOffset = currentY;
        setTranslate(currentX, currentY, dragItem);
      }
    }
    function setTranslate(xPos, yPos, el) {
      el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
    }
  }
  addCardOnBoard() {
    const board = document.querySelector(".plate");
    const cardo = document.createElement("div");
    cardo.classList.add("card-visit");
    cardo.classList.add(`${this.fields[0].doctor}-card`);
    cardo.setAttribute("data-id", `${this.id}`);
    cardo.setAttribute("draggable", "true");
    Visit.drag(cardo)
    cardo.innerHTML = `<button class="card-close-btn"><i class="fas fa-times icon-close"></i></button>
    <p class="card-doctor">${this.fields[0].doctor}</p>
    <button class="card-more_info-btn">Show more...</button>
    <div class="card-more_info"></div>
    `

    console.log(this.fields)
    this.fields.map(field => {
      let p = document.createElement('p');
      if (field.label) {
        console.log(field.label, field.str)
        p.classList.add(`card-${field.label}`)
        p.innerHTML = `${field.label}:${field.str}`;
        cardo.querySelector('.card-more_info').append(p)
      }
    })

    let e = document.createElement('button');
    e.classList.add('card-edit-btn');
    e.innerText = `edit`;
    cardo.querySelector('.card-more_info').append(e)

    board.append(cardo);
    Visit.showMoreInfo();
    e.addEventListener("click", this.editCard.bind(this));
    const cardCloseBtn = cardo.querySelector(".card-close-btn");
    cardCloseBtn.addEventListener("click", Visit.deleteCard)
  }
}

class Therapist extends Visit {
  constructor(options) {
    console.log(options)
    super(options);
    let { age } = options;
    this.age = age;
    this.fields = [{ doctor: "therapist" }, { label: 'name', str: this.name }, { label: 'title', str: this.title }, { label: 'age', str: this.age }, { label: 'description', str: this.description }];

  }

  async addCardOnServer() {
    const responce = await fetch('http://cards.danit.com.ua/cards', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(this)
    });
    const obj = await responce.json();
    this.id = obj.id;
    this.addCardOnBoard();

  }
}

class Stomatologist extends Visit {
  constructor(options) {
    super(options);
    let { date: lastDate } = options;
    this.lastDate = lastDate;
    this.fields = [{ doctor: "stomatologist" }, { label: 'name', str: this.name }, { label: 'title', str: this.title }, { label: 'date', str: this.date }, { label: 'description', str: this.description }];

  }
  async addCardOnServer() {
    const responce = await fetch('http://cards.danit.com.ua/cards', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(this)
    });
    const obj = await responce.json();
    this.id = obj.id;
    console.log(obj);


    this.addCardOnBoard();
  }
}

class Cardiologist extends Visit {
  constructor(options) {
    super(options);
    let { pressure, index, deseases, age } = options;
    this.pressure = pressure;
    this.index = index;
    this.deseases = deseases;
    this.age = age;
    this.fields = [{ doctor: "cardiologist" }, { label: 'name', str: this.name }, { label: 'title', str: this.title }, { label: 'age', str: this.age }, { label: 'pressure', str: this.pressure }, { label: 'index', str: this.index }, { label: 'deseases', str: this.deseases }, { label: 'description', str: this.description }];
    console.log(this)
  }

  async addCardOnServer() {
    const responce = await fetch('http://cards.danit.com.ua/cards', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(this)
    });
    const obj = await responce.json();
    console.log(obj);

    this.id = obj.id;
    this.addCardOnBoard();
  }
}