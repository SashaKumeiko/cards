const gulp = require('gulp'),
  css = require('gulp-css'),
  sync = require('browser-sync'),
  concat = require('gulp-concat'),
  clean = require('gulp-clean'),
  htmlmin = require('gulp-htmlmin'),
  minify = require('gulp-minify');

var gulp = require('gulp');
var ghPages = require('gulp-gh-pages');

const paths = {
  html: 'src/index.html',
  css: 'src/css/**/*.css',
  js: 'src/js/**/*.js',
  imgs: ['src/img/**/**.jpg', 'src/img/**/**.png'],
  distHtml: './dist',
  distCss: './dist/css',
  distJs: './dist/js',
  distImg: './dist/img',
};

gulp.task('deploy', function () {
  return gulp.src('./dist/**/*').pipe(ghPages());
});
gulp.task('scripts', function () {
  return gulp
    .src(paths.js)
    .pipe(concat('main.js'))
    .pipe(gulp.dest(paths.distJs))
    .pipe(sync.reload({stream: true}));
});

gulp.task('minscripts', function () {
  return gulp
    .src(paths.js)
    .pipe(concat('main.js'))
    .pipe(minify())
    .pipe(gulp.dest(paths.distJs));
});

gulp.task('css', function () {
  return gulp
    .src(paths.css)
    .pipe(gulp.dest(paths.distCss))
    .pipe(sync.reload({stream: true}));
});

gulp.task('mincss', function () {
  return gulp
    .src(paths.css)
    .pipe(css())
    .pipe(gulp.dest(paths.distCss))
    .pipe(sync.reload({stream: true}));
});

gulp.task('html', function () {
  return gulp
    .src(paths.html)
    .pipe(gulp.dest(paths.distHtml))
    .pipe(sync.reload({stream: true}));
});

gulp.task('minhtml', function () {
  return gulp
    .src(paths.html)
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(paths.distHtml));
});

gulp.task('imgs', function () {
  return gulp
    .src(paths.imgs)
    .pipe(gulp.dest(paths.distImg))
    .pipe(sync.reload({stream: true}));
});

gulp.task('watch', function () {
  gulp.watch(paths.css, gulp.series('css'));
  gulp.watch(paths.html, gulp.series('html'));
});

gulp.task('clean_dist', function () {
  return gulp.src('./dist', {read: false}).pipe(clean());
});

gulp.task('sync', function () {
  sync.init({
    server: {
      baseDir: 'dist',
    },
  });
});

gulp.task('start', gulp.series('css', 'html', 'scripts', 'imgs'));
gulp.task('build', gulp.series('minhtml', 'mincss', 'minscripts', 'imgs'));
gulp.task('work', gulp.series('clean_dist', 'css', 'html', 'scripts', 'imgs'));
gulp.task('watch_sync', gulp.parallel('watch', 'sync'));
gulp.task('default', gulp.series('work', 'watch_sync'));
